`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.10.2017 14:33:07
// Design Name: 
// Module Name: alternate_words
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alternate_words(
    input clk,
    input rst,
    
    input [63:0] din,
    input din_valid,
    output read_din,
    
    output [31:0] dout,
    output msb,
    input read_dout
  );

  reg high_word_reg;

  assign msb = high_word_reg;
  assign dout = high_word_reg ? din[63:32] : din[31:0];
  assign read_din = (read_dout && high_word_reg) ? 1 : 0;

  always @( posedge clk ) begin
    if (rst == 1'b1) begin
      high_word_reg <=  1'b0;
    end
    else begin
      if(read_dout) begin
        high_word_reg <= ~high_word_reg;
      end
      else begin
        high_word_reg <= high_word_reg;
      end
    end
  end


endmodule
