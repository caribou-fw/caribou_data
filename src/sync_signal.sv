`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.03.2018 15:15:00
// Design Name: 
// Module Name: sync_signal
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// signal clock domain crossing 
module sync_signal #(
    int WIDTH  = 1,
    int STAGES = 2 
)(
    input  out_clk,
    input  [WIDTH-1 : 0] signal_in,  // i, async
    output [WIDTH-1 : 0] signal_out  // o, sync to out_clk
    );



(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic [WIDTH-1 : 0] signal_sync [STAGES : 0]; // = {{STAGES{0}}};
//logic [WIDTH-1 : 0] signal_synced;

//assign signal_out = signal_synced;
assign signal_out = signal_sync[STAGES];

//VIVADO does not support assertions... #%*#&$!
//CHECK_MIN_STAGES: assert (STAGES >= 2) else $error("STAGES must be >= 2");

//genvar i;

    always_ff @(posedge out_clk) 
    begin
        //int i;
        signal_sync[0] <= signal_in;
        //generate
            for (int i=1; i <= STAGES; i=i+1)
                begin
                    signal_sync[i] <= signal_sync[i-1];
                end
        //endgenerate
        //signal_synced <= signal_sync[STAGES];

    end

endmodule
