
`timescale 1 ns / 1 ps

 module Caribou_data #
  (
    // Parameters of Axi Slave Bus Interface S00_AXI
    parameter integer C_S00_AXI_DATA_WIDTH  = 32,
    parameter integer C_S00_AXI_ADDR_WIDTH  = 5
  )
  (
    // DUT data ports
    input  wire        pdata_clk,   // parallel data clock input
    input  wire [63:0] pdata_in,    // paralel data input
    input  wire        pdata_write, // pdata_in valid flag
    input  wire        fifo_clear,  // clear data in FIFO (reset FIFO)
    output wire        fifo_empty,  //
    output wire        fifo_full,   //
    
    // Ports of Axi Slave Bus Interface S00_AXI
    input  wire                                  s00_axi_aclk,
    input  wire                                  s00_axi_aresetn,
    input  wire [C_S00_AXI_ADDR_WIDTH-1 : 0]     s00_axi_awaddr,
    input  wire [2 : 0]                          s00_axi_awprot,
    input  wire                                  s00_axi_awvalid,
    output wire                                  s00_axi_awready,
    input  wire [C_S00_AXI_DATA_WIDTH-1 : 0]     s00_axi_wdata,
    input  wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
    input  wire                                  s00_axi_wvalid,
    output wire                                  s00_axi_wready,
    output wire [1 : 0]                          s00_axi_bresp,
    output wire                                  s00_axi_bvalid,
    input  wire                                  s00_axi_bready,
    input  wire [C_S00_AXI_ADDR_WIDTH-1 : 0]     s00_axi_araddr,
    input  wire [2 : 0]                          s00_axi_arprot,
    input  wire                                  s00_axi_arvalid,
    output wire                                  s00_axi_arready,
    output wire [C_S00_AXI_DATA_WIDTH-1 : 0]     s00_axi_rdata,
    output wire [1 : 0]                          s00_axi_rresp,
    output wire                                  s00_axi_rvalid,
    input  wire                                  s00_axi_rready
  );

  wire fifo_empty_sync_axi;
  wire fifo_empty_sync_pdata;

  wire [63:0] fifo_data_out;
  wire        fifo_data_read;
  wire [31:0] axi_data_out;
  wire        axi_data_read;
  wire        fifo_msb;


  assign fifo_empty = fifo_empty_sync_pdata;

// Instantiation of Axi Bus Interface S00_AXI
  Caribou_data_AXI # ( 
    .C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
    .C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
  ) AXI_interface_inst (
    .fifo_data       (axi_data_out),         // i [31:0] data from FIFO to be read by software
    .fifo_data_valid (!fifo_empty_sync_axi), // i flag signalising that the data_out can be read
    .fifo_data_read  (axi_data_read),        // o read acknowledge flag - next data can be asserted
    .fifo_data_msb   (fifo_msb),             // i flag signalising the high 32 bits will be read
    .S_AXI_ACLK      (s00_axi_aclk),
    .S_AXI_ARESETN   (s00_axi_aresetn),
    .S_AXI_AWADDR    (s00_axi_awaddr),
    .S_AXI_AWPROT    (s00_axi_awprot),
    .S_AXI_AWVALID   (s00_axi_awvalid),
    .S_AXI_AWREADY   (s00_axi_awready),
    .S_AXI_WDATA     (s00_axi_wdata),
    .S_AXI_WSTRB     (s00_axi_wstrb),
    .S_AXI_WVALID    (s00_axi_wvalid),
    .S_AXI_WREADY    (s00_axi_wready),
    .S_AXI_BRESP     (s00_axi_bresp),
    .S_AXI_BVALID    (s00_axi_bvalid),
    .S_AXI_BREADY    (s00_axi_bready),
    .S_AXI_ARADDR    (s00_axi_araddr),
    .S_AXI_ARPROT    (s00_axi_arprot),
    .S_AXI_ARVALID   (s00_axi_arvalid),
    .S_AXI_ARREADY   (s00_axi_arready),
    .S_AXI_RDATA     (s00_axi_rdata),
    .S_AXI_RRESP     (s00_axi_rresp),
    .S_AXI_RVALID    (s00_axi_rvalid),
    .S_AXI_RREADY    (s00_axi_rready)
  );

// crossing signals from AXI clock domain to PDATA clock domain
  sync_signal # (
    .WIDTH(1),
    .STAGES(2) 
  ) sync_fifo_empty (
    .out_clk(pdata_clk),
    .signal_in(fifo_empty_sync_axi),    // i, sync to wr_clk
    .signal_out(fifo_empty_sync_pdata)  // o, sync to rd_clk
  );

  alternate_words alternate_words_inst (
    clk(s00_axi_aclk),
    rst(!s00_axi_aresetn),

    din(fifo_data_out),
    din_valid(!fifo_empty_sync_axi),
    read_din(fifo_data_read),

    dout(axi_data_out),
    msb(fifo_msb),
    read_dout(axi_data_read),
  );

  data_fifo data_fifo_inst(
    .rst(fifo_clear),           //: IN STD_LOGIC;
    .wr_clk(pdata_clk),         //: IN STD_LOGIC;
    .rd_clk(s00_axi_aclk),      //: IN STD_LOGIC;
    .din(pdata_in),             //: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    .wr_en(pdata_write),        //: IN STD_LOGIC;
    .rd_en(fifo_data_read),     //: IN STD_LOGIC;
    .dout(fifo_data_out),       //: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    .full(fifo_full),           //: OUT STD_LOGIC;
    .empty(fifo_empty_sync_axi) //: OUT STD_LOGIC
  );        




  endmodule
